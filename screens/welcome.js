import { Text, View, SafeAreaView, Image, TouchableOpacity } from "react-native";
import WelcomeImage from "../assets/images/home.png"
import { StatusBar } from "expo-status-bar";
import { useNavigation } from "@react-navigation/native";

const Welcome = () => {
    const navigation = useNavigation();
    return (
        <View className="absolute flex items-center justify-center bg-stone-100 h-full w-full">
            <StatusBar style="light" />
            <Image source={WelcomeImage} />
            <Text className="font-semibold mb-4 text-red-500" style={{ fontFamily: "Montserrat-Medium", fontSize: 20 }}>40K+ Premium recipes</Text>
            <Text className="font-semibold mb-4" style={{ fontFamily: "Montserrat-SemiBold" }}>Cook Like a Chef</Text>
            <TouchableOpacity onPress={() => navigation.navigate("Categories")}>
                <Text className="bg-red-500 text-white text-center text-lg w-52 p-4 rounded-2xl tracking-wider" style={{ fontFamily: "Montserrat-Bold" }}>Let's Go</Text>
            </TouchableOpacity>
        </View>
    )
}
export default Welcome;