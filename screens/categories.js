import { Text, View, SafeAreaView, FlatList, TextInput, ScrollView, TouchableOpacity, Image, Pressable } from "react-native";
import { StatusBar } from "expo-status-bar";
import * as Icons from "react-native-heroicons/solid";
import * as IconsOutline from "react-native-heroicons/outline";
import { colors, categories, recipeList } from "../Constant";
import { useState } from "react";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Categorylist from "../components/categoryList";


const Categories = () => {

    const [selectedCategory, setCategory] = useState("Breakfast");
    const [NotificationClick, setNotification] = useState(false);
    return (
        <View className="mt-10 bg-gray-100 p-4">
            <View>
                <StatusBar backgroundColor={"rgb(244 244 245)"} />
                <View className="flex-row justify-between mb-4">
                    <Text style={{fontFamily: "Montserrat-Bold", fontSize: 16 }}>Hi, John</Text>
                    <TouchableOpacity onPress={()=>setNotification(!NotificationClick)}>
                    {NotificationClick ? <Icons.BellIcon color={ colors.COLOR_PRIMARY} /> :  <IconsOutline.BellIcon color={ colors.COLOR_PRIMARY}/>}
                    </TouchableOpacity>
                    
                </View>
                <View className="bg-gray-200 mb-4 rounded-3xl flex justify-center p-4 text-neutral-100">
                    <Icons.MagnifyingGlassIcon className="absolute ml-4" color={colors.COLOR_PRIMARY} />
                    <TextInput className="text-neutral-600 ml-10" style={{fontFamily: "Montserrat-Medium", fontSize: 16 }}  placeholder="Enter the food" />
                </View>
                <View>
                    <Text className="py-1 text-neutral-600 mb-2" style={{fontFamily: "Montserrat-Bold", fontSize: 16 }}>
                        Categories
                    </Text>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        {categories.map((category, index) => {
                            return (
                                <TouchableOpacity onPress={() => setCategory(category.category)} key={index} className={`p-4 px-8 rounded-2xl m-3 bg-white`} style={{ backgroundColor: selectedCategory === category.category ? colors.COLOR_PRIMARY : "#fff", fontFamily: "Montserrat-SemiBold" }}>
                                    <Text  className="py-1 text-neutral-600" style={{ color: selectedCategory === category.category ? '#fff' : "black", fontFamily: "Montserrat-SemiBold", fontSize: 15 }}>{category.category}</Text>
                                </TouchableOpacity>
                            )
                        })}
                    </ScrollView>

                </View>
            </View>
            <View>
                <Categorylist />
            </View>

        </View>
    )
}
export default Categories;