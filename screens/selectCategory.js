import { Text, View, SafeAreaView, Image, TouchableOpacity, ScrollView } from "react-native";
import { StatusBar } from "expo-status-bar";
import * as Icons from "react-native-heroicons/solid";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { useNavigation } from "@react-navigation/native";
import { useState } from "react";

const SelectedCategory = ({ route }) => {
    const item = route.params;
    const navigation = useNavigation();
    const [favourite, setFavourite] = useState(false);

    return (
        <SafeAreaView>
            <StatusBar hidden={true} />
            <View className="flex-row justify-between p-4" style={{ backgroundColor: item.color, height: hp(25) }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icons.ArrowLeftCircleIcon color={"white"} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setFavourite(!favourite)}>
                    <Icons.HeartIcon color={favourite ? "red" : "white"} />
                </TouchableOpacity>
            </View>
            <View className="absolute mt-32 bg-white" style={{
                borderTopLeftRadius: 56,
                borderTopRightRadius: 56,
            }}>
                <View className="flex items-center mt-8">
                    <Image source={item.image} style={{ width: wp(40), height: hp(20) }} />
                </View>
                <Text className="text-center font-semibold" style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20 }}>{item.name}</Text>
                <ScrollView style={{ height: hp(60) }} showsVerticalScrollIndicator={false}>
                    <View className="p-5">
                        <View>
                            <Text className="text-neutral-600" style={{ fontFamily: "Montserrat-Medium", fontSize: 14 }}>{item.description}</Text>
                        </View>
                        <View className="flex-row justify-between my-5">
                            <View className="p-8 rounded-2xl" style={{ backgroundColor: "rgba(255, 0, 0, 0.38)" }}>
                                <Text style={{ fontSize: 40 }}>⏰</Text>
                                <Text className="py-1 text-neutral-600" style={{ fontFamily: "Montserrat-SemiBold", fontSize: 14 }}>{item.time}</Text>
                            </View>
                            <View className="p-8 rounded-2xl" style={{ backgroundColor: "rgba(135, 206, 235, 0.8)" }}>
                                <Text style={{ fontSize: 40 }}>🥣</Text>
                                <Text className="py-1 text-neutral-600" style={{ fontFamily: "Montserrat-SemiBold", fontSize: 14 }}>{item.difficulty}</Text>
                            </View>
                            <View className="p-8 rounded-2xl" style={{ backgroundColor: "rgba(255, 165, 0, 0.48)" }}>
                                <Text style={{ fontSize: 40 }}>🔥</Text>
                                <Text className="py-1 text-neutral-600" style={{ fontFamily: "Montserrat-SemiBold", fontSize: 14 }}>{item.calories}</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20 }}>Ingredients :</Text>
                            <View className="p-4">
                                {item?.ingredients.map((val, index) => {
                                    return (
                                        <View className="flex-row items-center gap-3">
                                            <Text style={{ fontSize: 12 }}>🔴</Text>
                                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 14 }} key={index} className="py-1 text-neutral-600">{val}</Text>
                                        </View>

                                    )
                                })}
                            </View>
                        </View>
                        <View className="my-5">
                            <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 20 }}>Steps :</Text>
                            <View className="p-4">
                                {item?.steps.map((val, index) => {
                                    return (
                                        <View className="flex-row">
                                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 14 }} className="py-1 text-neutral-600">{index + 1}.&nbsp; </Text>
                                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: 14 }} className="py-1 text-neutral-600">{val}</Text>
                                        </View>
                                    )
                                })}
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    )
}
export default SelectedCategory;