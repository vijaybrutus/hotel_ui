import { colors, categories, recipeList  } from "../Constant";
import { useNavigation } from "@react-navigation/native";
import { Text, View, Image, Pressable, FlatList, SafeAreaView } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const Categorylist=()=>{

    const navigation = useNavigation();

    return(
        <View className="h-full" style={{height:hp(73)}}>
        <FlatList
            data={recipeList}
            renderItem={({ item }) => (
                <Pressable
                    onPress={() => navigation.navigate("SelectedCategory", { ...item })}
                    style={{
                        backgroundColor: colors.COLOR_LIGHT,
                        shadowColor: "#000",
                        shadowOffset: { width: 0, height: 4 },
                        shadowOpacity: 0.1,
                        shadowRadius: 7,
                        borderRadius: 16,
                        marginVertical: 16,
                        alignItems: "center",
                        paddingHorizontal: 8,
                        paddingVertical: 26,
                      
                    }}
                >
                    <Image
                        source={item.image}
                        style={{ width: 150, height: 150, resizeMode: "center" }}
                    />
                    <Text className="text-neutral-600" style={{fontFamily: "Montserrat-Medium", fontSize: 14 }}>{item.name}</Text>
                    <View style={{ flexDirection: "row", marginTop: 8 }}>
                        <Text className="text-neutral-600" style={{fontFamily: "Montserrat-Medium", fontSize: 14 }}>{item.time}</Text>
                        <Text> | </Text>
                        <View style={{ flexDirection: "row" }}>
                            <Text className="text-neutral-600" style={{fontFamily: "Montserrat-Medium", fontSize: 14, marginRight: 4 }}>{item.rating}</Text>
                        </View>
                    </View>
                </Pressable>
            )}
            numColumns={2}
            columnWrapperStyle={{
                justifyContent: "space-between",
            }}
            showsVerticalScrollIndicator={false}
        />
   
    </View>
    )
}
export default Categorylist;