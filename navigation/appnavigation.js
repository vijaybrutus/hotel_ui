import { View, Text } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Welcome from "../screens/welcome";
import Categories from "../screens/categories";
import SelectedCategory from "../screens/selectCategory";
const Stack = createNativeStackNavigator();
const AppNavigation=()=>{
      
    return(
        <NavigationContainer>
        <Stack.Navigator initialRouteName="Welcome" screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Welcome" component={Welcome} />
          <Stack.Screen name="Categories" component={Categories} />
          <Stack.Screen name="SelectedCategory" component={SelectedCategory} />
        </Stack.Navigator>
      </NavigationContainer>
    )
}
export default AppNavigation;