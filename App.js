import { StatusBar } from 'expo-status-bar';
import { SafeAreaView, Text, View } from 'react-native';
import AppNavigation from './navigation/appnavigation';
import { useFonts } from 'expo-font';
import MontserratBlack from './assets/fonts/Montserrat-Black.ttf';
import MontserratBold from './assets/fonts/Montserrat-Bold.ttf';
import MontserratExtraBold from './assets/fonts/Montserrat-ExtraBold.ttf';
import MontserratExtraLight from './assets/fonts/Montserrat-ExtraLight.ttf';
import MontserratLight from './assets/fonts/Montserrat-Light.ttf';
import MontserratMedium from './assets/fonts/Montserrat-Medium.ttf';
import MontserratRegular from './assets/fonts/Montserrat-Regular.ttf';
import MontserratSemiBold from './assets/fonts/Montserrat-SemiBold.ttf';
import MontserratThin from './assets/fonts/Montserrat-Thin.ttf';

export default function App() {
  const [fontsLoaded] = useFonts({
    'Montserrat-Black': MontserratBlack,
    'Montserrat-Bold': MontserratBold,
    'Montserrat-ExtraBold': MontserratExtraBold,
    'Montserrat-ExtraLight': MontserratExtraLight,
    'Montserrat-Light': MontserratLight,
    'Montserrat-Medium': MontserratMedium,
    'Montserrat-Regular': MontserratRegular,
    'Montserrat-SemiBold': MontserratSemiBold,
    'Montserrat-Thin': MontserratThin
  })
  if(!fontsLoaded){
    return undefined
  }
  return (
    <SafeAreaView className="flex-1">
          <AppNavigation />
    </SafeAreaView>
    
  );
}

